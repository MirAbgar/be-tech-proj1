package me.mirabgar.betechproj1.core.service.model.response;

public record Address(
    String country,
    String city,
    String street,
    String postalCode
) {

}
