package me.mirabgar.betechproj1.core.error;

import me.mirabgar.betechproj1.core.constant.ErrorReasons;

public class PassportAlreadyExistsException extends BtpAbstractException {

    public PassportAlreadyExistsException() {
        super("This passport is already registered", ErrorReasons.BAD_REQUEST);
    }
}
