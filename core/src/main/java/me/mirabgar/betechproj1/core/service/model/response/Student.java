package me.mirabgar.betechproj1.core.service.model.response;

import me.mirabgar.betechproj1.core.repository.entity.enumeration.Faculty;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Nationality;
import java.util.Date;

public record Student(
    String id,
    String firstName,
    String lastName,
    String fullName,
    Address address,
    String email,
    String mobileNumber,
    Date birthDate,
    String passportNumber,
    Nationality nationality,
    Date created,
    Date updated,
    Integer year,
    Date graduated,
    Faculty faculty
) {

}
