package me.mirabgar.betechproj1.core.error;

import me.mirabgar.betechproj1.core.constant.ErrorReasons;

public class EmailAlreadyExistsException extends BtpAbstractException {

    public EmailAlreadyExistsException() {
        super("This email is already registered", ErrorReasons.BAD_REQUEST);
    }
}
