package me.mirabgar.betechproj1.core.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ErrorReasons {

    public final String BAD_REQUEST = "bad_request";
}
