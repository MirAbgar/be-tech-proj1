package me.mirabgar.betechproj1.core.error;

import lombok.Getter;

@Getter
abstract public class BtpAbstractException extends RuntimeException {

    protected String message;
    protected String reason;

    public BtpAbstractException(String message, String reason) {
        this.message = message;
        this.reason = reason;
    }
}
