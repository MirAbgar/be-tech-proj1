package me.mirabgar.betechproj1.core.service;

import lombok.RequiredArgsConstructor;
import me.mirabgar.betechproj1.core.error.EmailAlreadyExistsException;
import me.mirabgar.betechproj1.core.error.PassportAlreadyExistsException;
import me.mirabgar.betechproj1.core.repository.mongo.StudentRepository;
import me.mirabgar.betechproj1.core.service.model.mapper.StudentModelMapper;
import me.mirabgar.betechproj1.core.service.model.request.StudentCreateRequestModel;
import me.mirabgar.betechproj1.core.service.model.response.Student;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public Student insertStudent(StudentCreateRequestModel studentCreateRequestModel) {
        final var studentEntity = StudentModelMapper.mapToStudentEntity(studentCreateRequestModel);
        if (studentRepository.existsByPassportNumber(studentEntity.getPassportNumber())) {
            throw new PassportAlreadyExistsException();
        }
        if (studentRepository.existsByEmail(studentEntity.getEmail())) {
            throw new EmailAlreadyExistsException();
        }
        final var insertedStudent = studentRepository.insert(studentEntity);
        return StudentModelMapper.mapToStudent(insertedStudent);
    }
}
