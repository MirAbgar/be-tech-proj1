package me.mirabgar.betechproj1.core.service.model.request;

import java.util.Date;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Faculty;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Nationality;

public record StudentCreateRequestModel(
    String firstName,
    String lastName,
    AddressModel address,
    String email,
    String mobileNumber,
    Date birthDate,
    String passportNumber,
    Nationality nationality,
    Faculty faculty
) {

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
