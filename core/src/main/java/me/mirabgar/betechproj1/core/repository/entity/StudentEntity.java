package me.mirabgar.betechproj1.core.repository.entity;

import java.util.Date;
import lombok.Data;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Faculty;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Nationality;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document("students")
public class StudentEntity {

    @Id
    private String id;
    @Field("first_name")
    private final String firstName;
    @Field("last_name")
    private final String lastName;
    @Field("full_name")
    private final String fullName;
    @Field("address")
    private final AddressEntity address;
    @Field("email")
    @Indexed(unique = true)
    private final String email;
    @Field("mobile_number")
    private final String mobileNumber;
    @Field("birth_date")
    private final Date birthDate;
    @Field("passportNumber")
    @Indexed(unique = true)
    private final String passportNumber;
    @Field("nationality")
    private final Nationality nationality;
    @Field("created")
    @CreatedDate
    private Date created;
    @Field("updated")
    @LastModifiedDate
    private Date updated;
    @Field("year")
    private final Integer year = 1;
    @Field("graduated")
    private Date graduated;
    @Field("faculty")
    private final Faculty faculty;

    public StudentEntity(String firstName, String lastName, String fullName, AddressEntity address, String email,
        String mobileNumber, Date birthDate, String passportNumber, Nationality nationality, Faculty faculty) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
        this.address = address;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.birthDate = birthDate;
        this.passportNumber = passportNumber;
        this.nationality = nationality;
        this.faculty = faculty;
    }
}
