package me.mirabgar.betechproj1.core.repository.entity.enumeration;

public enum Faculty {
    CS,
    MARKETING,
    FINANCE,
    LAW,
    MANAGEMENT
}
