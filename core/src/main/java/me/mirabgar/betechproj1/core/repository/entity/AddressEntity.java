package me.mirabgar.betechproj1.core.repository.entity;

import org.springframework.data.mongodb.core.mapping.Field;

public record AddressEntity(
    @Field("country")
    String country,
    @Field("city")
    String city,
    @Field("street")
    String street,
    @Field("postal_code")
    String postalCode
) {

}
