package me.mirabgar.betechproj1.core.repository.mongo;

import me.mirabgar.betechproj1.core.repository.entity.StudentEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentRepository extends MongoRepository<StudentEntity, String> {

    boolean existsByEmail(String email);
    
    boolean existsByPassportNumber(String passportNumber);
}
