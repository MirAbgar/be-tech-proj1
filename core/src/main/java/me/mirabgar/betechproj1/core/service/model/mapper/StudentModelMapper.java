package me.mirabgar.betechproj1.core.service.model.mapper;

import lombok.experimental.UtilityClass;
import me.mirabgar.betechproj1.core.repository.entity.AddressEntity;
import me.mirabgar.betechproj1.core.repository.entity.StudentEntity;
import me.mirabgar.betechproj1.core.service.model.request.AddressModel;
import me.mirabgar.betechproj1.core.service.model.request.StudentCreateRequestModel;
import me.mirabgar.betechproj1.core.service.model.response.Address;
import me.mirabgar.betechproj1.core.service.model.response.Student;

@UtilityClass
public class StudentModelMapper {

    public StudentEntity mapToStudentEntity(StudentCreateRequestModel studentCreateRequestModel) {
        return new StudentEntity(
            studentCreateRequestModel.firstName(),
            studentCreateRequestModel.lastName(),
            studentCreateRequestModel.getFullName(),
            mapToAddressEntity(studentCreateRequestModel.address()),
            studentCreateRequestModel.email(),
            studentCreateRequestModel.mobileNumber(),
            studentCreateRequestModel.birthDate(),
            studentCreateRequestModel.passportNumber(),
            studentCreateRequestModel.nationality(),
            studentCreateRequestModel.faculty()
        );
    }

    public Student mapToStudent(StudentEntity studentEntity) {
        return new Student(
            studentEntity.getId(),
            studentEntity.getFirstName(),
            studentEntity.getLastName(),
            studentEntity.getFullName(),
            mapToAddress(studentEntity.getAddress()),
            studentEntity.getEmail(),
            studentEntity.getMobileNumber(),
            studentEntity.getBirthDate(),
            studentEntity.getPassportNumber(),
            studentEntity.getNationality(),
            studentEntity.getCreated(),
            studentEntity.getUpdated(),
            studentEntity.getYear(),
            studentEntity.getGraduated(),
            studentEntity.getFaculty()
        );
    }

    private AddressEntity mapToAddressEntity(AddressModel addressModel) {
        return new AddressEntity(
            addressModel.country(),
            addressModel.city(),
            addressModel.street(),
            addressModel.postalCode()
        );
    }
    
    private Address mapToAddress(AddressEntity addressEntity) {
        return new Address(
            addressEntity.country(),
            addressEntity.city(),
            addressEntity.street(),
            addressEntity.postalCode()
        );
    }
}
