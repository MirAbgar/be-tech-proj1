package me.mirabgar.betechproj1.core.repository.entity.enumeration;

public enum Nationality {
    AFGHAN,
    ALBANIAN,
    ALGERIAN,
    AMERICAN,
    ANDORRAN,
    ARMENIAN,
    AUSTRALIAN,
    BRAZILIAN,
    BRITISH,
    CUBAN,
    CHINESE,
    CANADIAN,
    CZECH,
    DUTCH,
    DANISH,
    IRANIAN,
    IRISH,
    ITALIAN,
    INDIAN,
    GREEK,
    FRENCH,
    YEMENI
    // etc.
}
