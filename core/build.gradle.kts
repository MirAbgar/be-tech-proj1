plugins {
    id("subproject.btp-java-library")
    alias(libs.plugins.spring.dependency.management)
}

dependencies {
    implementation(platform(libs.spring.boot.dependencies))
    
    api("org.springframework.boot:spring-boot-starter-data-mongodb")

    compileOnlyApi("org.projectlombok:lombok")

    annotationProcessor(platform(libs.spring.boot.dependencies))
    annotationProcessor("org.projectlombok:lombok")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
}
