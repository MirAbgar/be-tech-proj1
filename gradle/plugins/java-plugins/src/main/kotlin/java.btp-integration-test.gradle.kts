import gradle.kotlin.dsl.accessors._7758f57c6dd35933dbd9dc8c103ba8e9.sourceSets

plugins {
    id("idea")
}

sourceSets {
    create("integration") {
        java.srcDir("$projectDir/src/integration/java")
        resources.srcDir("$projectDir/src/integration/resources")
        compileClasspath += sourceSets["main"].compileClasspath + sourceSets["test"].compileClasspath
        runtimeClasspath += sourceSets["main"].runtimeClasspath + sourceSets["test"].runtimeClasspath
//        compileClasspath += sourceSets["main"].output + sourceSets["test"].output
//        runtimeClasspath += sourceSets["main"].output + sourceSets["test"].output
    }
}

idea {
    module {
        testSources.from(sourceSets["integration"].java.srcDirs)
        testResources.from(sourceSets["integration"].resources.srcDirs)
    }
}

tasks.register<Test>("integrationTest") {
    group = "verification"
    testClassesDirs = sourceSets["integration"].output.classesDirs
    classpath = sourceSets["integration"].runtimeClasspath
}

tasks.named("check") {
    dependsOn("integrationTest")
}
