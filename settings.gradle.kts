rootProject.name = "be-tech-proj1"

// applications
include("rest")

// libraries
include("core")

dependencyResolutionManagement {
    repositories.mavenCentral()
}

pluginManagement {
    includeBuild("gradle/plugins")
}
