package me.mirabgar.betechproj1;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import me.mirabgar.betechproj1.base.BaseIntegrationTest;
import me.mirabgar.betechproj1.core.constant.ErrorReasons;
import me.mirabgar.betechproj1.core.repository.entity.StudentEntity;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Faculty;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Nationality;
import me.mirabgar.betechproj1.rest.controller.model.request.AddressDto;
import me.mirabgar.betechproj1.rest.controller.model.request.StudentCreateRequest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;

class StudentControllerTest extends BaseIntegrationTest {
    
    @Test
    @DisplayName("should insert student")
    void shouldInsertStudent() throws Exception {
        // Given
        final var student = mockStudentCreateRequest();
        
        // When
        // Then
        assertThat(mongoTemplate.count(new Query(), StudentEntity.class)).isZero();

        mockMvc.perform(
            post("/api/v1/students")
                .content(asJsonString(student))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.first_name").value(student.firstName()))
            .andExpect(jsonPath("$.last_name").value(student.lastName()))
            .andExpect(jsonPath("$.full_name").value(student.firstName() + " " + student.lastName()))
            .andExpect(jsonPath("$.address.country").value(student.address().country()))
            .andExpect(jsonPath("$.address.city").value(student.address().city()))
            .andExpect(jsonPath("$.address.street").value(student.address().street()))
            .andExpect(jsonPath("$.address.postal_code").value(student.address().postalCode()))
            .andExpect(jsonPath("$.email").value(student.email()))
            .andExpect(jsonPath("$.mobile_number").value(student.mobileNumber()))
            .andExpect(jsonPath("$.birth_date").value(toZuluUtc(student.birthDate())))
            .andExpect(jsonPath("$.passport_number").value(student.passportNumber()))
            .andExpect(jsonPath("$.nationality").value(student.nationality().toString()))
            .andExpect(jsonPath("$.created").exists())
            .andExpect(jsonPath("$.updated").exists())
            .andExpect(jsonPath("$.year").value(1))
            .andExpect(jsonPath("$.graduated").doesNotExist())
            .andExpect(jsonPath("$.faculty").value(student.faculty().toString()));
        
        assertThat(mongoTemplate.count(new Query(), StudentEntity.class)).isOne();
    }
    
    @Test
    @DisplayName("should fail when inserting user with existing email")
    void shouldFailWhenInsertingUserWithExistingEmail() throws Exception {
        // Given
        final var email = "alreadyExisting@gmail.com";
        final var existingStudent = mockStudentCreateRequest(email, "df122521");
        insertStudent(existingStudent);
        final var student = mockStudentCreateRequest(email, "ab527773");

        // When
        // Then
        assertThat(mongoTemplate.count(new Query(), StudentEntity.class)).isOne();

        mockMvc.perform(
                post("/api/v1/students")
                    .content(asJsonString(student))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("This email is already registered"))
            .andExpect(jsonPath("$.reason").value(ErrorReasons.BAD_REQUEST));

        assertThat(mongoTemplate.count(new Query(), StudentEntity.class)).isOne();
    }

    @Test
    @DisplayName("should fail when inserting user with existing passport number")
    void shouldFailWhenInsertingUserWithExistingPassportNumber() throws Exception {
        // Given
        final var passportNumber = "existing-passport-number";
        final var existingStudent = mockStudentCreateRequest("mail1@gmail.com", passportNumber);
        insertStudent(existingStudent);
        final var student = mockStudentCreateRequest("mail2@gmail.com", passportNumber);
        
        // When
        // Then
        assertThat(mongoTemplate.count(new Query(), StudentEntity.class)).isOne();

        mockMvc.perform(
                post("/api/v1/students")
                    .content(asJsonString(student))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").value("This passport is already registered"))
            .andExpect(jsonPath("$.reason").value(ErrorReasons.BAD_REQUEST));
        
        assertThat(mongoTemplate.count(new Query(), StudentEntity.class)).isOne();
    }
    
    private void insertStudent(StudentCreateRequest student) throws Exception {
        mockMvc.perform(
                post("/api/v1/students")
                    .content(asJsonString(student))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk());
    }
    
    private StudentCreateRequest mockStudentCreateRequest() {
        return mockStudentCreateRequest(faker.internet().emailAddress(), faker.number().digits(10));
    }
    
    private StudentCreateRequest mockStudentCreateRequest(String email, String passportNumber) {
        return new StudentCreateRequest(
            faker.name().firstName(),
            faker.name().lastName(),
            new AddressDto(
                faker.address().country(),
                faker.address().city(),
                faker.address().streetName(),
                faker.address().zipCode()
            ),
            email,
            faker.phoneNumber().phoneNumber(),
            new Date(),
            passportNumber,
            Nationality.ANDORRAN,
            Faculty.CS
        );
    }
}
