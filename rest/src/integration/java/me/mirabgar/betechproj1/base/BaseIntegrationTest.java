package me.mirabgar.betechproj1.base;

import com.github.javafaker.Faker;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import me.mirabgar.betechproj1.core.repository.entity.StudentEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import org.testcontainers.utility.DockerImageName;

/**
 * source: <a href="https://spring.io/guides/gs/testing-web/">This is for MockMvc part and why it is used for tests</a>
 */
@SpringBootTest
@AutoConfigureMockMvc
public abstract class BaseIntegrationTest {

    private static final MongoDBContainer mongoDbContainer =
        new MongoDBContainer(DockerImageName.parse("mongo:latest"));

    @Value("${spring.jackson.date-format}")
    private String dateFormat;
    
    @Value("${spring.jackson.time-zone}")
    private String timeZone;
    
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

    private final ObjectMapper objectMapper = new ObjectMapper();

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry propertyRegistry) {
        propertyRegistry.add("spring.data.mongodb.uri", mongoDbContainer::getReplicaSetUrl);
    }

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected MongoTemplate mongoTemplate;
    
    protected final Faker faker = new Faker();

    @BeforeAll
    public static void beforeAll() {
        mongoDbContainer.start();
    }

    @AfterEach
    void tearDown() {
        mongoTemplate.remove(StudentEntity.class).all();
    }

    protected String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
    
    protected String toZuluUtc(final Date date) {
        simpleDateFormat.applyPattern(dateFormat);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        return simpleDateFormat.format(date);
    }
}
