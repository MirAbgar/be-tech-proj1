package me.mirabgar.betechproj1.rest.error;

import me.mirabgar.betechproj1.core.error.EmailAlreadyExistsException;
import me.mirabgar.betechproj1.core.error.PassportAlreadyExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {
    
    private static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";

    @ExceptionHandler
    public ResponseEntity<BtpFailureResponse> handleDefault(Exception e) {
        return new ResponseEntity<>(
            new BtpFailureResponse("Internal Server Error", INTERNAL_SERVER_ERROR),
            HttpStatus.INTERNAL_SERVER_ERROR
        );
    }
    
    @ExceptionHandler
    public ResponseEntity<BtpFailureResponse> handlePassportAlreadyExistsException(PassportAlreadyExistsException e) {
        return new ResponseEntity<>(
            new BtpFailureResponse(e.getMessage(), e.getReason()),
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler
    public ResponseEntity<BtpFailureResponse> handlePassportAlreadyExistsException(EmailAlreadyExistsException e) {
        return new ResponseEntity<>(
            new BtpFailureResponse(e.getMessage(), e.getReason()),
            HttpStatus.BAD_REQUEST
        );
    }
}
