package me.mirabgar.betechproj1.rest.controller.model.mapper;

import lombok.experimental.UtilityClass;
import me.mirabgar.betechproj1.core.service.model.request.AddressModel;
import me.mirabgar.betechproj1.core.service.model.request.StudentCreateRequestModel;
import me.mirabgar.betechproj1.core.service.model.response.Address;
import me.mirabgar.betechproj1.core.service.model.response.Student;
import me.mirabgar.betechproj1.rest.controller.model.request.AddressDto;
import me.mirabgar.betechproj1.rest.controller.model.request.StudentCreateRequest;
import me.mirabgar.betechproj1.rest.controller.model.response.StudentDto;

@UtilityClass
public class StudentDtoMapper {

    public StudentCreateRequestModel mapToStudentCreateModel(StudentCreateRequest studentCreateRequest) {
        return new StudentCreateRequestModel(
            studentCreateRequest.firstName(),
            studentCreateRequest.lastName(),
            mapToAddressModel(studentCreateRequest.address()),
            studentCreateRequest.email(),
            studentCreateRequest.mobileNumber(),
            studentCreateRequest.birthDate(),
            studentCreateRequest.passportNumber(),
            studentCreateRequest.nationality(),
            studentCreateRequest.faculty()
        );
    }

    public StudentDto mapToStudentDto(Student student) {
        return new StudentDto(
            student.id(),
            student.firstName(),
            student.lastName(),
            student.fullName(),
            mapToAddressDto(student.address()),
            student.email(),
            student.mobileNumber(),
            student.birthDate(),
            student.passportNumber(),
            student.nationality(),
            student.created(),
            student.updated(),
            student.year(),
            student.graduated(),
            student.faculty()
        );
    }

    private AddressModel mapToAddressModel(AddressDto addressDto) {
        return new AddressModel(addressDto.country(), addressDto.city(), addressDto.street(), addressDto.postalCode());
    }

    private me.mirabgar.betechproj1.rest.controller.model.response.AddressDto mapToAddressDto(Address address) {
        return new me.mirabgar.betechproj1.rest.controller.model.response.AddressDto(
            address.country(),
            address.city(),
            address.street(),
            address.postalCode()
        );
    }
}
