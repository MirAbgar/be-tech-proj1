package me.mirabgar.betechproj1.rest.error;

import com.fasterxml.jackson.annotation.JsonProperty;

public record BtpFailureResponse(
    @JsonProperty("message")
    String message,
    @JsonProperty("reason")
    String reason
) {

}
