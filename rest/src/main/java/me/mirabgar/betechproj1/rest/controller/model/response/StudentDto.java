package me.mirabgar.betechproj1.rest.controller.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Faculty;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Nationality;
import java.util.Date;

public record StudentDto(
    @JsonProperty("id")
    String id,
    @JsonProperty("first_name")
    String firstName,
    @JsonProperty("last_name")
    String lastName,
    @JsonProperty("full_name")
    String fullName,
    @JsonProperty("address")
    AddressDto address,
    @JsonProperty("email")
    String email,
    @JsonProperty("mobile_number")
    String mobileNumber,
    @JsonProperty("birth_date")
    Date birthDate,
    @JsonProperty("passport_number")
    String passportNumber,
    @JsonProperty("nationality")
    Nationality nationality,
    @JsonProperty("created")
    Date created,
    @JsonProperty("updated")
    Date updated,
    @JsonProperty("year")
    Integer year,
    @JsonProperty("graduated")
    Date graduated,
    @JsonProperty("faculty")
    Faculty faculty
) {

}
