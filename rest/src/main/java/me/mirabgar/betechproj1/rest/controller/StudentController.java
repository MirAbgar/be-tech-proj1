package me.mirabgar.betechproj1.rest.controller;

import lombok.RequiredArgsConstructor;
import me.mirabgar.betechproj1.core.service.StudentService;
import me.mirabgar.betechproj1.rest.controller.model.mapper.StudentDtoMapper;
import me.mirabgar.betechproj1.rest.controller.model.request.StudentCreateRequest;
import me.mirabgar.betechproj1.rest.controller.model.response.StudentDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
    value = "/api/v1/students",
    produces = MediaType.APPLICATION_JSON_VALUE
)
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public StudentDto insertStudent(@RequestBody StudentCreateRequest request) {
        final var student = studentService.insertStudent(StudentDtoMapper.mapToStudentCreateModel(request));
        return StudentDtoMapper.mapToStudentDto(student);
    }
}
