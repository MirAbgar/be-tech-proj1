package me.mirabgar.betechproj1.rest.controller.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public record AddressDto(
    @JsonProperty("country")
    String country,
    @JsonProperty("city")
    String city,
    @JsonProperty("street")
    String street,
    @JsonProperty("postal_code")
    String postalCode
) {

}
