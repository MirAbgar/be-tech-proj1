package me.mirabgar.betechproj1.rest.controller.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

import me.mirabgar.betechproj1.core.repository.entity.enumeration.Faculty;
import me.mirabgar.betechproj1.core.repository.entity.enumeration.Nationality;

public record StudentCreateRequest(
    @JsonProperty("first_name")
    String firstName,
    @JsonProperty("last_name")
    String lastName,
    @JsonProperty("address")
    AddressDto address,
    @JsonProperty("email")
    String email,
    @JsonProperty("mobile_number")
    String mobileNumber,
    @JsonProperty("birth_date")
    Date birthDate,
    @JsonProperty("passport_number")
    String passportNumber,
    @JsonProperty("nationality")
    Nationality nationality,
    @JsonProperty("faculty")
    Faculty faculty
) {

}
