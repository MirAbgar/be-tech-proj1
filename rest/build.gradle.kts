plugins {
    id("subproject.btp-java-application")
    alias(libs.plugins.spring.boot)
    alias(libs.plugins.spring.dependency.management)
}

version = "0.0.1-SNAPSHOT"

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

dependencies {
    implementation(project(":core"))

    implementation("org.springframework.boot:spring-boot-starter-web")

    developmentOnly("org.springframework.boot:spring-boot-docker-compose")

    annotationProcessor("org.projectlombok:lombok")

    integrationImplementation("org.springframework.boot:spring-boot-starter-test")
    integrationImplementation("org.springframework.boot:spring-boot-testcontainers")
    integrationImplementation("org.testcontainers:mongodb")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation(libs.java.faker) {
        exclude(module = "snakeyaml")
    }
}

tasks.bootJar {
    mainClass.set("me.mirabgar.betechproj1.RestApplication")
}
